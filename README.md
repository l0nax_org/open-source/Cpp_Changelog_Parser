# C/C++ Changelog Parser
<p align="center">
	
	<a href="https://gitlab.com/l0nax_org/open-source/Cpp_Changelog_Parser/wikis/"><b>Wiki</b></a>
</p>


Contributing
============
To report bugs and suggest new feature use the
[issue tracker][issues].  If you have some code which you would like
to be merged, then open a pull request. Please also see
[CONTRIBUTING.md][contrib].

Releases
========
**Coming Soon**

Donations
=========
<!-- ToDo -->

<hr>
[![license](https://img.shields.io/github/license/mashape/apistatus.svg)](https://choosealicense.com/licenses/mit/)

## Reports
**Master** <br/>
[![pipeline status](https://gitlab.com/l0nax_org/open-source/Cpp_Changelog_Parser/badges/master/pipeline.svg)](https://gitlab.com/l0nax_org/open-source/Cpp_Changelog_Parser/commits/master)
[![coverage report](https://gitlab.com/l0nax_org/open-source/Cpp_Changelog_Parser/badges/master/coverage.svg)](https://gitlab.com/l0nax_org/open-source/Cpp_Changelog_Parser/commits/master)

**Developer** <br/>
[![pipeline status](https://gitlab.com/l0nax_org/open-source/Cpp_Changelog_Parser/badges/develop/pipeline.svg)](https://gitlab.com/l0nax_org/open-source/Cpp_Changelog_Parser/commits/develop)
[![coverage report](https://gitlab.com/l0nax_org/open-source/Cpp_Changelog_Parser/badges/develop/coverage.svg)](https://gitlab.com/l0nax_org/open-source/Cpp_Changelog_Parser/commits/develop)

<!-- ============ -->
[contrib]: https://gitlab.com/l0nax_org/open-source/Cpp_Changelog_Parser/blob/master/CONTRIBUTING.md
[issues]:  https://gitlab.com/l0nax_org/open-source/Cpp_Changelog_Parser/issues



